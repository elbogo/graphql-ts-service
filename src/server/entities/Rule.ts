import { 
  Entity, Column, BaseEntity, PrimaryGeneratedColumn, ManyToOne,
  ObjectID, ObjectIdColumn,
} from "typeorm";
import { Field, ObjectType, ID } from "type-graphql";

import { User } from "./User";

@ObjectType()
@Entity()
export class Rule extends BaseEntity {
  /**
   * have to set to ID and ObjectID
   * specifically for mongodb
   */
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ObjectIdColumn()
  readonly id: ObjectID;

  @Field()
  @Column("text", { unique: true })
  url: string;

  @Column()
  frequency: string;

  @Field(() => User)
  @ManyToOne(() => User)
  author: User;
  @Column({ nullable: true })
  authorId: number;
}
