import "reflect-metadata";
import { createConnection, getConnectionOptions } from "typeorm";
import express from "express";
import session from "express-session";
import mongoose from "mongoose";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { resolvers } from "./resolvers";

const PROD:boolean = process.env.NODE_ENV === "production";
const DEV:boolean = process.env.NODE_ENV === "development";
if (DEV) require('dotenv').config()

console.log('process.env.NODE_ENV', process.env.NODE_ENV);
console.log('process.env.MONGODB_URI', process.env.MONGODB_URI);

//connect to MongoDB
const MongoStore = require('connect-mongo')(session);
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI || '', { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;

//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('db connected'));

(async () => {
  const app = express();

  app.use(
    session({
      store: new MongoStore({
          mongooseConnection: db
      }),
      name: "sid",
      secret: process.env.SESSION_SECRET || "rockNroll5",
      resave: false,
      saveUninitialized: false,
      cookie: {
        httpOnly: true,
        secure: PROD,
        maxAge: 1000 * 60 * 60 * 24 * 365 // 1 year
      }
    })
  );

  // get options from ormconfig.js
  const dbOptions = await getConnectionOptions(PROD ? 'production' : 'development');
  await createConnection({ ...dbOptions, name: "default" });

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers,
      validate: false
    }),
    context: ({ req, res }) => ({ req, res })
  });

  apolloServer.applyMiddleware({ app, cors: false });
  const port = process.env.PORT;
  app.listen(port, () => {
    console.log(`server started at http://localhost:${port}/graphql`);
  });
})();
