import { ObjectType, Field } from "type-graphql";
import { FieldError } from "./FieldError";
import { Rule } from "../entities/Rule";

@ObjectType()
export class RuleResponse {
  @Field(() => Rule, { nullable: true })
  rule?: Rule;

  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];
}
