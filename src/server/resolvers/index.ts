import { AuthResolver } from './AuthResolver'
import { RuleResolver } from './RuleResolver'

export const resolvers = [
    AuthResolver,
    RuleResolver,
]