module.exports = [
  {
    name: "development",
    type: "mongodb",
    url: process.env.MONGODB_URI,
    synchronize: true,
    logging: true,
    entities: ["src/server/entities/**/*.ts"],
    migrations: ["src/server/migrations/**/*.ts"],
    subscribers: ["src/server/subscribers/**/*.ts"],
    cli: {
      entitiesDir: "src/server/entities",
      migrationsDir: "src/server/migrations",
      subscribersDir: "src/server/subscribers"
    }
  },
  {
    name: "production",
    type: "mongodb",
    url: process.env.MONGODB_URI,
    // TODO: switch this to false to use migrations after initial launch
    synchronize: true, 
    logging: false,
    entities: ["dist/server/entities/**/*.js"],
    migrations: ["dist/server/migrations/**/*.js"],
    subscribers: ["dist/server/subscribers/**/*.js"],
    cli: {
      entitiesDir: "dist/server/entities",
      migrationsDir: "dist/server/migrations",
      subscribersDir: "dist/server/subscribers"
    }
  }
];
