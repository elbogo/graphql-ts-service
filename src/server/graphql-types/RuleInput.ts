import { InputType, Field } from "type-graphql";

@InputType()
export class RuleInput {
  @Field()
  url: string;

  @Field()
  frequency: string;
}
