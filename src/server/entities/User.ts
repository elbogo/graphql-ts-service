import { 
  Entity, Column, BaseEntity, ObjectID, ObjectIdColumn, PrimaryGeneratedColumn 
} from "typeorm";
import { Field, ObjectType, ID } from "type-graphql";

@ObjectType()
@Entity()
export class User extends BaseEntity {
  /**
   * have to set to ID and ObjectID
   * specifically for mongodb
   */
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ObjectIdColumn()
  readonly id: ObjectID;

  @Field()
  @Column("text", { unique: true })
  email: string;

  @Column()
  password: string;
}
