import { Query, Resolver, Mutation, Arg, UseMiddleware } from "type-graphql";
import { isAuth } from "../middlewares/isAuth";

import { Rule } from '../entities/Rule';
import { RuleInput } from '../graphql-types/RuleInput';
import { RuleResponse } from '../graphql-types/RuleResponse';

@Resolver()
export class RuleResolver {
  @Query(() => String)
  @UseMiddleware(isAuth)
  test() {
    return "Test Passed";
  }

  @Mutation(() => RuleResponse)
  async createRule(
    @Arg("input")
    { url, frequency }: RuleInput
  ): Promise<RuleResponse> {

    if (!url || !frequency) {
      return {
        errors: [
          {
            path: "rule",
            message: "no url or frequency"
          }
        ]
      };
    }

    const rule = await Rule.create({
      url,
      frequency
    }).save();

    return { rule };
  }
}
